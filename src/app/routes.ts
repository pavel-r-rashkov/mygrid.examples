import { OverviewComponent } from './overview/overview.component';
import { SortingComponent } from './sorting/sorting.component';
import { GroupingComponent } from './grouping/grouping.component';
import { FilteringComponent } from './filtering/filtering.component';
import { PagingComponent } from './paging/paging.component';
import { CustomTemplatesComponent } from './custom-templates/custom-templates.component';
import { HtmlTableElementsComponent } from './html-table-elements/html-table-elements.component';
import { SummaryComponent } from './summary/summary.component';
import { AddEditItemComponent } from './add-edit-item/add-edit-item.component';
import { FormValidationComponent } from './form-validation/form-validation.component';
import { ServerSideModeComponent } from './server-side-mode/server-side-mode.component';
import { MasterDetailComponent } from './master-detail/master-detail.component';
import { ScrollableGridComponent } from './scrollable-grid/scrollable-grid.component';

export const routes: any[] = [
  {
    path: 'overview',
    component: OverviewComponent
  },
  {
    path: 'sorting',
    component: SortingComponent
  },
  {
    path: 'grouping',
    component: GroupingComponent
  },
  {
    path: 'filtering',
    component: FilteringComponent
  },
  {
    path: 'paging',
    component: PagingComponent
  },
  {
    path: 'custom-templates',
    component: CustomTemplatesComponent
  },
  {
    path: 'html-elements',
    component: HtmlTableElementsComponent
  },
  {
    path: 'summary',
    component: SummaryComponent
  },
  {
    path: 'add-edit-item',
    component: AddEditItemComponent
  },
  {
    path: 'form-validation',
    component: FormValidationComponent
  },
  {
    path: 'server-side-mode',
    component: ServerSideModeComponent
  },
  {
    path: 'master-detail',
    component: MasterDetailComponent
  },
  {
    path: 'scrollable-grid',
    component: ScrollableGridComponent
  }
];