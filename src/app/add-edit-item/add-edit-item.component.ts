import { Component } from '@angular/core';
import { Observable } from 'rxjs/Rx';

import { DataService } from '../data.service';


@Component({
  moduleId: module.id,
  selector: 'mge-add-edit-item',
  templateUrl: './add-edit-item.component.html'
})
export class AddEditItemComponent { 
  users: any[];
  showAddRow: boolean = true;
  showAddRowToggle: any = true;
  saveChanges: (changes: any) => Observable<any>;
  deleteItem: (itemKey) => Observable<any>
  simulateError: boolean;

  constructor(
    private data: DataService) {

    this.users = this.data.getUsers();
    this.saveChanges = (changes: any) => {
      // You can modify the item here or call a web service to persist the changes.
      // If an error occurs the changes won't be applied
      console.log('Changes:', changes);

      if (changes.isNewItem) {
        // generate a key for the new item or get it from the server.
        // always generate a key for new items, otherwise some functionalities won't work properly
        changes.item['id'] = Math.ceil(Math.random() * 1000);
      }

      if (this.simulateError) {
        return Observable.throw(new Error('An error occured'));
      }
      return Observable.empty();
    };

    this.deleteItem = (itemKey) => {
      console.log('Attempting to delete item with key:', itemKey);
      if (this.simulateError) {
        return Observable.throw(new Error('An error occured'));
      }
      return Observable.empty();
    };
  }
}