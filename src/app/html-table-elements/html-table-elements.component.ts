import { 
  Component,
  ElementRef,
  Renderer2
} from '@angular/core';

import { DataService } from '../data.service';
import { HtmlElementType } from 'my-grid';

@Component({
  moduleId: module.id,
  selector: 'mge-html-table-elements',
  templateUrl: 'html-table-elements.component.html'
})
export class HtmlTableElementsComponent {
  users: any[];
  initCell: (context: any, elementRef: ElementRef, type: HtmlElementType) => void;

  constructor(
    private data: DataService,
    private renderer: Renderer2) {

    this.users = this.data.getUsers();
    this.initCell = (context: any, elementRef: ElementRef, type: HtmlElementType) => {
      if (type == HtmlElementType.DataCell && context.data.propertyName == 'age') {
        var age = context.item[context.data.propertyName];
        if (age < 30) {
          this.renderer.addClass(elementRef.nativeElement, 'under-30');
        }
      } else if (type == HtmlElementType.HeaderCell && context.headerData.propertyName == 'lastName') {
        this.renderer.addClass(elementRef.nativeElement, 'last-name-header');
      } else if (type == HtmlElementType.FilterCell && context.filterData.propertyName == 'firstName') {
        this.renderer.addClass(elementRef.nativeElement, 'first-name-filter');
      } else if (type == HtmlElementType.EditCell && context.data.propertyName == 'birthdate') {
        this.renderer.addClass(elementRef.nativeElement, 'birthdate-edit');
      }
    };
  }
}