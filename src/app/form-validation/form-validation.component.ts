import { Component } from '@angular/core';
import { Validators } from '@angular/forms';

import { DataService } from '../data.service';

@Component({
  moduleId: module.id,
  selector: 'mge-form-validation',
  templateUrl: 'form-validation.component.html'
})
export class FormValidationComponent {
  users: any[];
  showAddRow: boolean = true;
  initForm: (editForm: any) => void;
  getErrorMessages: (errorKeys: any, propertyName: string) => string[];

  constructor(
    private data: DataService) {

    this.users = this.data.getUsers();
    this.initForm = (formData: any) => {
      console.log('Form:', formData);
      formData.form.controls.age.validator = Validators.required;
    };

    this.getErrorMessages = (errorKeys: any, propertyName: string) => {
      var messages = [];
      if (propertyName == 'age' && errorKeys['required']) {
        messages.push('Age is required');
      }

      return messages;
    };
  }


}