import { Component } from '@angular/core';
import { Observable } from 'rxjs/Rx';

import { DataService } from '../data.service';
import { GridState } from 'my-grid';

@Component({
  moduleId: module.id,
  selector: 'mge-server-side-mode',
  templateUrl: './server-side-mode.component.html'
})
export class ServerSideModeComponent {
  isGroupingEnabled: boolean = true;
  isServerSideModeEnabled: boolean = true;
  pageSize: number = 3;
  isPerPageAgeSummary: boolean = false;
  currentState: GridState;
  gridStateChange: (gridState: GridState) => Observable<any>;

  constructor() {
    this.gridStateChange = (gridState: GridState) => {
      console.log('Grid state:', gridState);
      this.currentState = gridState;
      // Send the grid state to a web service and process it on the server side
      // Return only the results that need to be displayed as well as the count of all filtered results and global summary results (summary items with isPerPageSummary set to false)

      // Static data just for the demo
      return Observable.of({
        items: [{}, {}, {}],
        count: 8,
        summaryResults: [{ summaryType: 'sum', propertyName: 'age', summaryValue: 123 }]
      });
    };
  }
}