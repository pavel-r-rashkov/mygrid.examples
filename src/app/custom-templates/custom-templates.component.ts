import { Component } from '@angular/core';
import { Validators } from '@angular/forms';

import { DataService } from '../data.service';

@Component({
	moduleId: module.id,
	selector: 'mge-custom-templates',
	templateUrl: 'custom-templates.component.html'
})
export class CustomTemplatesComponent {
	users: any[];
	isGroupingEnabled: boolean = true;
	initForm: (editForm: any) => void;

	constructor(
		private data: DataService) {

		this.users = this.data.getUsers();
		this.initForm = (formData: any) => {
      formData.form.controls.age.validator = Validators.required;
    };
	}

	filterAge(ageFilterContext: any, minAge: number, maxAge: number) {
		// don't filter if only minAge or only maxAge is supplied
		if ((!minAge && !!maxAge)
			|| (!!minAge && !maxAge)) {
			return;
		}

		var appliedFilters = {
			minAge: minAge,
			maxAge: maxAge
		};

		var propertyName = (!minAge && !maxAge) ? null : ageFilterContext.filterData.propertyName;
		ageFilterContext.filter(propertyName, appliedFilters);
	}

	customFilterCallback(val: any, filterData: any, item: any): boolean {
		if (!val) {
			return false;
		}

		return val >= filterData.minAge && val <= filterData.maxAge;
	}

	changePage(context, pageDiff) {
		var newPage = context.paginationState.currentPage + pageDiff;
		let maxPages = Math.ceil(context.itemsCount / context.paginationState.pageSize);

		if (newPage < 1 || newPage > maxPages) {
			return;
		}

		context.changePage(newPage, context.paginationState.pageSize);
	}

	shouldRenderValidationErrors(validationContext: any) {
		return validationContext.editForm.get([validationContext.data.propertyName]).invalid
			&& (validationContext.editForm.get([validationContext.data.propertyName]).dirty 
				|| validationContext.editForm.dirty);
	}
}