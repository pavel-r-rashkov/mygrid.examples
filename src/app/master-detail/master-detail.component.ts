import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Rx';

import { DataService } from '../data.service';

@Component({
  moduleId: module.id,
  selector: 'mge-master-detail',
  templateUrl: 'master-detail.component.html'
})
export class MasterDetailComponent implements OnInit {
  users: any[];
  detailGridPageSize: number = 5;
  getDetailItems: (masterKey: any, masterItem: any) => Observable<any[]>

  constructor(
    private data: DataService) {

    this.users = this.data.getUsers();

    // It is not required to specify getDetailItems. In some cases you will have a lot of details
    // and it will be better to enable the server side mode of the details grid.
    this.getDetailItems = (masterKey: any, masterItem: any) => {
      // get the details from an in-memory collection or call a web service to get them
      // in this example we are getting the details form the master item itself
      return Observable.of(masterItem['orders']);
    };
  }

  ngOnInit() {
    
  }
}