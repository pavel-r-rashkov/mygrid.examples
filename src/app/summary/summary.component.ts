import { 
  Component
} from '@angular/core';

import { DataService } from '../data.service';

@Component({
  moduleId: module.id,
  selector: 'mge-summary',
  templateUrl: 'summary.component.html'
})
export class SummaryComponent {
  users: any[];
  pageSize: number = 5;
  isPerPageSummary: boolean = false;
  customCalculateSummary: (items: any[]) => any;

  constructor(
    private data: DataService) {

    this.users = this.data.getUsers();
    this.customCalculateSummary = (items: any[]) => {
      var usersAbove30 = items.filter((item) => {
        return item['age'] >= 30;
      });

      var ageSum = usersAbove30.reduce((prev, current, index, items: any[]) => {
        return prev + current['age'];
      }, 0);

      return ageSum / usersAbove30.length;
    };
  }
}