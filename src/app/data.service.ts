import { Injectable } from '@angular/core';

@Injectable()
export class DataService {
	getUsers() {
		let users = [
			{ 
				id: 1, 
				firstName: 'John', 
				lastName: 'Doe', 
				age: 24, 
				birthdate: new Date(1993, 2, 12),
				orders: [
					{ id: 234, description: 'Foo', date: new Date(), sum: 1.23 },
					{ id: 1123, description: 'Bar', date: new Date(), sum: 7.79 },
					{ id: 567, description: 'Foo', date: new Date(), sum: 1.23 },
					{ id: 9283, description: 'Bar', date: new Date(), sum: 7.79 },
					{ id: 9453, description: 'Foo', date: new Date(), sum: 1.23 },
					{ id: 291, description: 'Bar', date: new Date(), sum: 7.79 },
					{ id: 7643, description: 'Foo', date: new Date(), sum: 1.23 },
					{ id: 76238, description: 'Bar', date: new Date(), sum: 7.79 }
				]
			},
			{ 
				id: 2, 
				firstName: 'Peter', 
				lastName: 'Donovan', 
				age: 32, birthdate: 
				new Date(1985, 3, 23), 
				orders: [] 
			},
			{ 
				id: 3, 
				firstName: 'Steven', 
				lastName: 'Isner', 
				age: 34, 
				birthdate: new Date(1983, 6, 17), 
				orders: [
					{ id: 1991, description: 'Foo', date: new Date(), sum: 127.00 },
					{ id: 8292, description: 'Bar', date: new Date(), sum: 9.99 }
				] 
			},
			{ 
				id: 4, 
				firstName: 'Robert', 
				lastName: 'Johnson', 
				age: 34, 
				birthdate: new Date(1983, 1, 12), 
				orders: [] 
			},
			{ 
				id: 5, 
				firstName: 'Ann', 
				lastName: 'Tylor', 
				age: 32, 
				birthdate: new Date(1985, 6, 2), 
				orders: [] 
			},
			{ 
				id: 6, 
				firstName: 'David', 
				lastName: 'Robinson', 
				age: 25, 
				birthdate: new Date(1992, 1, 28), 
				orders: [
					{ id: 72728, description: 'Foo', date: new Date(), sum: 287.00 },
					{ id: 56226, description: 'Bar', date: new Date(), sum: 19.99 }
				] 
			},
			{ 
				id: 7, 
				firstName: 'Josh', 
				lastName: 'Smith', 
				age: 37, 
				birthdate: new Date(1980, 3, 12), 
				orders: []
			}
		];

		return users;
	}
}