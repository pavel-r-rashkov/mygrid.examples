import { Component } from '@angular/core';

import { DataService } from '../data.service';

@Component({
	moduleId: module.id,
	selector: 'mge-overview',
	templateUrl: 'overview.component.html'
})
export class OverviewComponent {
	users: any[];

	constructor(
		private data: DataService) {
		this.users = this.data.getUsers();
	}
}