import { Component } from '@angular/core';

import { DataService } from '../data.service';

@Component({
	moduleId: module.id,
	selector: 'mge-paging',
	templateUrl: 'paging.component.html'
})
export class PagingComponent {
	users: any[];
	pageSize: number = 5;

	constructor(
		private data: DataService) {
		this.users = this.data.getUsers();
	}

  beforePagingCallback(users: any[]) {
    console.log('Users before paging:', users);
  }
}