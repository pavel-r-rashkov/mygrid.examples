import { Component } from '@angular/core';

import { DataService } from '../data.service';

@Component({
  moduleId: module.id,
  selector: 'mge-scrollable-grid',
  templateUrl: 'scrollable-grid.component.html'
})
export class ScrollableGridComponent {
  users: any[];

  constructor(
    private data: DataService) {
    this.users = this.data.getUsers();
  }
}