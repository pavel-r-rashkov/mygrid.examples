import { Component } from '@angular/core';

import { DataService } from '../data.service';

@Component({
	moduleId: module.id,
	selector: 'mge-sorting',
	templateUrl: 'sorting.component.html'
})
export class SortingComponent {
	users: any[];
	isOrderingEnabled: boolean = false;

	constructor(
		private data: DataService) {
		this.users = this.data.getUsers();
	}

	beforeSortingCallback(users: any[]) {
		console.log('Users before being sorted:', users);
	}

	customOrderCallback(firstValue: any, secondValue: any, isAscending: boolean, firstItem: any, secondItem: any) {
		let firstDay = firstValue.getDate();
		let secondDay = secondValue.getDate();
		if (firstDay == secondDay) {
			return 0;
		}

		let order = firstDay < secondDay ? -1 : 1;
		return isAscending ? order : -order;
	}
}