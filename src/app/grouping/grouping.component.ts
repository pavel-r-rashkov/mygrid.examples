import { Component } from '@angular/core';

import { DataService } from '../data.service';

@Component({
	moduleId: module.id,
	selector: 'mge-grouping',
	templateUrl: 'grouping.component.html'
})
export class GroupingComponent {
	users: any[];
	isGroupingEnabled = true;
	isLastNameGroupingEnalbed = false;

	constructor(
		private data: DataService) {
		this.users = this.data.getUsers();
	}

	beforeGroupingCallback(users: any[]) {
		console.log('Users before being grouped', users);
	}

	customGroupCallback(firstValue: any, secondValue: any, isAscending: boolean, firstItem: any, secondItem: any) {
		let firstYear = firstValue.getYear();
		let secondYear = secondValue.getYear();

		if (firstYear == secondYear) {
			return 0;
		}

		let grouping = firstYear < secondYear ? -1 : 1;
		return isAscending ? grouping : -grouping;
	}
}