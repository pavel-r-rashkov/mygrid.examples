import { Component } from '@angular/core';

import { DataService } from '../data.service';

@Component({
	moduleId: module.id,
	selector: 'mge-filtering',
	templateUrl: 'filtering.component.html'
})
export class FilteringComponent {
	users: any[];
	isFilteringEnabled: boolean = false;

	constructor(
		private data: DataService) {
		this.users = this.data.getUsers();
	}

	beforeFilteringCallback(users: any[]) {
		console.log('Users before being filtered:', users);
	}

	customFilterCallback(val: any, filterData: any, item: any): boolean {
		let year = val.getFullYear();
		let filterYear = new Date(filterData.selectedDate).getFullYear();
		return year == filterYear;
	}
}