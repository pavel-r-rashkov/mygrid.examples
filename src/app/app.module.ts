import { BrowserModule } from '@angular/platform-browser';
import { RouterModule }   from '@angular/router';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { MyGridModule } from 'my-grid';

import { AppComponent } from './app.component';
import { DataService } from './data.service';
import { OverviewComponent } from './overview/overview.component';
import { SortingComponent } from './sorting/sorting.component';
import { GroupingComponent } from './grouping/grouping.component';
import { FilteringComponent } from './filtering/filtering.component';
import { PagingComponent } from './paging/paging.component';
import { CustomTemplatesComponent } from './custom-templates/custom-templates.component';
import { HtmlTableElementsComponent } from './html-table-elements/html-table-elements.component';
import { SummaryComponent } from './summary/summary.component';
import { AddEditItemComponent } from './add-edit-item/add-edit-item.component';
import { FormValidationComponent } from './form-validation/form-validation.component';
import { ServerSideModeComponent } from './server-side-mode/server-side-mode.component';
import { MasterDetailComponent } from './master-detail/master-detail.component';
import { ScrollableGridComponent } from './scrollable-grid/scrollable-grid.component';
import { routes } from './routes';

@NgModule({
  declarations: [
    AppComponent,
    OverviewComponent,
    SortingComponent,
    GroupingComponent,
    FilteringComponent,
    PagingComponent,
    CustomTemplatesComponent,
    HtmlTableElementsComponent,
    SummaryComponent,
    AddEditItemComponent,
    FormValidationComponent,
    ServerSideModeComponent,
    MasterDetailComponent,
    ScrollableGridComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    RouterModule.forRoot(routes),
    MyGridModule
  ],
  providers: [
    DataService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
