import { MyGridExamplesPage } from './app.po';

describe('my-grid-examples App', () => {
  let page: MyGridExamplesPage;

  beforeEach(() => {
    page = new MyGridExamplesPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
